import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MainTest {

    @Test
    public void factorialTest() {
        assertEquals(120, Main.factorial(5), "5! = 120");
        assertEquals(24, Main.factorial(4), "4! = 24");
        assertEquals(6, Main.factorial(3), "3! = 6");
        assertNotEquals(120, Main.factorial(-5), "-8!");
    }

    @Test
    public void someMethod() {
        TipaInterface ti = Mockito.mock(TipaInterface.class);
        Main main = new Main(ti);
        when(ti.foo(5))
                .thenReturn(true);

        assertTrue(main.someMethod(5));

        verify(ti).foo(5);
    }

    @Test
    public void fibonacci() {
        Main main = new Main();

        assertArrayEquals(new int[]{1, 1, 2, 3, 5}, main.fibonacci(5));

        assertThrows(IllegalArgumentException.class, () -> main.fibonacci(0));

    }
}
