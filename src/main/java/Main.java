public class Main {


    private TipaInterface tipaInterface;

    public Main(TipaInterface ti) {
        this.tipaInterface = ti;
    }

    public Main() {

    }

    public static int factorial(int n) {
        if (n < 0) return -1;
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }

    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    public boolean someMethod(int k) {
        final boolean foo = tipaInterface.foo(k);
        return foo;
    }

    public int[] fibonacci(int n) {
        if (n <= 2) throw new IllegalArgumentException();
        int[] seq = new int[n];
        seq[0] = seq[1] = 1;
        for (int i = 2; i < n; i++) {
            seq[i] = seq[i - 2] + seq[i - 1];
        }
        return seq;
    }
}
